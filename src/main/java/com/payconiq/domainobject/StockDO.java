package com.payconiq.domainobject;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

public class StockDO {
	
	@SequenceGenerator(name="stock_id_gen",  sequenceName="stock_id_seq", initialValue = 1, allocationSize = 1)
	@Id	@GeneratedValue(generator="stock_id_gen")
	private Long id;
	
	@NotNull(message = "Name parameter missing!")
	@Column(length = 100, unique = true, nullable = false)
	private String name;
	
	@NotNull(message="Quantity parameter missing!")
	@Digits(integer=11, fraction=0, message="Quantity number exceeded!")
	private Integer quantity;
	
	@NotNull(message="Current price parameter missing!")
	@Digits(integer=11, fraction=2, message="Current price exceeded!")
	@Column(name = "current-price", nullable = false)
	private BigDecimal currentPrice;
	
	@Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime created = ZonedDateTime.now();
	
	@Column(nullable = false)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime lastUpdated = ZonedDateTime.now();

}
